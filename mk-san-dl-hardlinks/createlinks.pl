#!/usr/bin/perl
use strict;
use DBI;
use Config::PHPINI;

my $conf = Config::PHPINI->read('/etc/opt/pelagos.ini');
my $db_conf = Config::PHPINI->read("$conf->{paths}->{conf}/db.ini");

my %dbi_type_map = ('postgresql' => 'Pg', 'mysql' => 'mysql');
my %db = %{$db_conf->{'GOMRI_RO'}};
my $dbh = DBI->connect("DBI:$dbi_type_map{$db{type}}:host=$db{host};port=$db{port};dbname=$db{dbname};",$db{'username'},$db{'password'},{'RaiseError' => 1});

my $sql = "select access_status from registry_view where dataset_udi = ?";
my $sth=$dbh->prepare($sql);

opendir(DIR,$conf->{'paths'}->{'data_store'}) or die "Cannot open $conf->{paths}->{data_store}\n";
my @udis = readdir(DIR);
closedir(DIR);
foreach my $udi (@udis) {
    next if ($udi =~ /^\.$/);
    next if ($udi =~ /^\.\.$/);
    # clean old links from previous run - perms may have changed
    if ( -d "$conf->{paths}->{data_download}/$udi" ) {
        `rm -rf "$conf->{paths}->{data_download}/$udi"`;
    }
    $sth->execute($udi);
    my $restricted = 1;
    if (my @row = $sth->fetchrow_array()) {
        if($row[0] eq 'None') {
            $restricted = 0;
        }
    }
    if ($restricted == 0) { 
        print "creating public $conf->{paths}->{data_download}/$udi\n";
        mkdir "$conf->{paths}->{data_download}/$udi", 0751;
        `chown apache.apache $conf->{paths}->{data_download}/$udi`;
    } else {
        print "creating secure $conf->{paths}->{data_download}/$udi\n";
        mkdir "$conf->{paths}->{data_download}/$udi", 0750;
        `chown apache.apache $conf->{paths}->{data_download}/$udi`;
    }
    # give GRIIDC group access
    system("setfacl -m g:GRIIDC:r-x $conf->{paths}->{data_download}/$udi");
    opendir(UDIDIR, "$conf->{paths}->{data_store}/$udi") or die "Can't open $udi directory\n";
    my @files = readdir(UDIDIR);
    closedir(UDIDIR);
    foreach my $file (@files) {
        if ( ($file =~ /^.*\.dat$/) or ($file =~ /^.*\.met$/) ) {
            link "$conf->{paths}->{data_store}/$udi/$file","$conf->{paths}->{data_download}/$udi/$file";
            chmod 0644,"$conf->{paths}->{data_download}/$udi/$file";
        }
    }
}
$sth->finish();
$dbh->disconnect();
