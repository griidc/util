#!/usr/bin/perl
# Williamson
# This script removes hardlinks used by the GridFTP distribution process after
# a pre-determined amount of time configured per TB.  It is intended to be called
# via find(1) and scheduled as a cronjob.
#
# example usage: /bin/find /sftp/data/GridFTP -name "*.createdon" -exec /usr/local/bin/HardlinkPruner.pl {} \;
# Since this runs as root and deletes files specified by a non-root user,
# it makes sure the file matches the starting location for the GridFTP distribution
# directory.  (and also disallows use of ..). 
use strict;
use POSIX;

my $days_per_tb = 7;

my $now=time();
open (IFILE, $ARGV[0]);
while(<IFILE>) {
    chomp();
    print "RAW: $_\n";
    # SAFETY CHECK - prevent hijacking of deleter script by input file
    if(($_ =~ /^\/sftp\/data\/GridFTP\//) and ($_ !=~ /\.\./)){
        my($file,$created,$size) = split(/\|/,$_);
        print "file: $file\n";
        print "created: ".localtime($created)."\n";
        print "size: $size bytes\n";
        # Increase filesize by one byte in case filesize is zero, giving zero length
        # files the same minimal life.
        my $expires_in_seconds = ceil(($size+1)/1000000000000)*$days_per_tb*24*3600;
        my $expires_at = $created+$expires_in_seconds;
        if (($created+$expires_in_seconds) < $now) {  
            print "Link expired on ".localtime($expires_at).", so removing it.\n\n";
            unlink $file;
            unlink $ARGV[0];
        } else {
            print "Link not yet expired. Expires at ".localtime($expires_at)."\n\n";
        }
    } else {
        print "Safety check failed.  Refusing to delete: $_\n";
    }
}
close IFILE;
