#!/usr/bin/perl -w

use strict;

use Config::PHPINI;
use Getopt::Long;
use Net::LDAP;
use Time::Local;
use Date::Format;
use MIME::Lite;

my $main_config_file = '/etc/opt/pelagos.ini';
# make sure main config file exists
-f $main_config_file or die "missing main config file: $main_config_file";
# load main config file
my $conf = Config::PHPINI->read($main_config_file);
# make sure conf path is defined
(defined $conf->{'paths'}->{'conf'} and $conf->{'paths'}->{'conf'} ne '') or die "missing conf path in: $main_config_file";

# make sure mail config file exists
-f "$conf->{paths}->{conf}/mail.ini" or die "missing SMTP config file: $conf->{paths}->{conf}/mail.ini";
# load mail config file
%{$conf} = ( %{$conf}, %{Config::PHPINI->read("$conf->{paths}->{conf}/mail.ini")} );
# make sure all required fields from stmp are defined
for my $key ('smtp_server','delay','from','reply-to') {
    (defined $conf->{'mail'}->{$key} and $conf->{'mail'}->{$key} ne '') or die "missing configuration parameter in $conf->{paths}->{conf}/mail.ini: [mail] $key";
}

my $logfile;
my $test = 0;
my $to_address;
my $warn_days = '7,6,5,4,3,2,1,0';

# get command line options
GetOptions(
    'log=s' => \$logfile,
    'test' => \$test,
    'to_address=s' => \$to_address,
    'smtp_server=s' => \$conf->{'mail'}->{'smtp_server'},
    'delay=i' => \$conf->{'mail'}->{'delay'},
    'warn_days=s' => \$warn_days
);

my @warn_days = split /[, ]+/, $warn_days;

my $boilerplate_text = <<'EOT';
Passwords must meet the following requirements:
* must be a minimum of 8 characters long
* must contain at least three of the following:
  * an uppercase character
  * a lowercase character
  * a base 10 digit (0-9)
  * a non-alphanumeric character: ~ ! @ # $ % ^ & * _ - + = ` | \ ( ) { } [ ] : ; " ' < > , . ? /
  * any alphabetic character that is not uppercase or lowercase (including characters from Asian languages)

If you have any questions or concerns about your GRIIDC Account, please contact: griidc@gomri.org
EOT

my $boilerplate_html = <<'EOT';
<p>
    Passwords must meet the following requirements:
    <ul>
        <li>must be a minimum of 8 characters long</li>
        <li>must contain at least three of the following:
            <ul>
                <li>an uppercase character</li>
                <li>a lowercase character</li>
                <li>a base 10 digit (0-9)</li>
                <li>a non-alphanumeric character: ~ ! @ # $ % ^ &amp; * _ - + = ` | \ ( ) { } [ ] : ; " ' &lt; &gt; , . ? /</li>
                <li>any alphabetic character that is not uppercase or lowercase (including characters from Asian languages)</li>
            </ul>
        </li>
    </ul>
</p>
<p>If you have any questions or concerns about your GRIIDC Account, please contact: <a href="mailto:griidc@gomri.org">griidc@gomri.org</a></p>
EOT

open LOG, '>>', $logfile or die $! if defined $logfile;

my $ldap = Net::LDAP->new('triton.tamucc.edu') or die "$@";

my $mesg = $ldap->bind();

$mesg = $ldap->search(
                       base   => "cn=default,ou=pwpolicies,dc=griidc,dc=org",
                       filter => "(objectClass=*)",
                       attrs => ['pwdMaxAge','pwdExpireWarning']
                     );

$mesg->code && die $mesg->error;

my $ppolicy = ($mesg->entries)[0];
my $pwdMaxAge = $ppolicy->get_value('pwdMaxAge');
my $pwdExpireWarning = $ppolicy->get_value('pwdExpireWarning');

$mesg = $ldap->search(
                       base   => "ou=members,ou=people,dc=griidc,dc=org",
                       filter => "(objectClass=inetOrgPerson)",
                       attrs => ['uid','cn','mail','pwdChangedTime']
                     );
 
$mesg->code && die $mesg->error;

my @mail;
 
foreach my $entry ($mesg->entries) { 
    my $uid = ($entry->get_value('uid'))[0];
    my $name = ($entry->get_value('cn'))[0];
    my $mail = ($entry->get_value('mail'))[0];
    my ($year,$month,$mday,$hour,$min,$sec) = ($entry->get_value('pwdChangedTime'))[0] =~ /(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})/;
    my $pwdChangedTime = timegm($sec,$min,$hour,$mday,$month-1,$year);

    my $pwdExpireTime = $pwdChangedTime + $pwdMaxAge;

    my $send_mail = 0;
    my $subject;
    my $msg_text;
    my $msg_html;
    my $logmsg;
    my $tstamp = `date +"%c"`; chomp($tstamp);

    if ($pwdExpireTime < time) {
        # password expired
        if (time - $pwdExpireTime < 86410) {
            # password expired in the last 24 hours (+10 seconds to account for script run time)
            $subject = 'Your GRIIDC Account password has expired';
            $msg_text = <<"EOT";
Dear $name,

In order to comply with rules and procedures of Texas A&M University-Corpus Christi, our home institution, your GRIIDC Account password has expired.

GRIIDC Username: $uid
Associated Email Address: $mail

To reset your password, please visit: https://data.gulfresearchinitiative.org/account/password

$boilerplate_text
EOT
            $msg_html = <<"EOT";
<p>Dear $name,</p>
<p>In order to comply with rules and procedures of Texas A&amp;M University-Corpus Christi, our home institution, your GRIIDC Account password has expired.</p>
<p>GRIIDC Username: $uid<br>Associated Email Address: $mail</p>
<p>To reset your password, please visit: <a href="https://data.gulfresearchinitiative.org/account/password">https://data.gulfresearchinitiative.org/account/password</a></p>
$boilerplate_html
EOT
            $logmsg = "$tstamp \"$name\" <$mail> was sent a message informing them that their password has expired.\n";
        }
    }
    elsif ($pwdExpireTime - $pwdExpireWarning < time) {
        # account is in the warning period
        $subject = 'Your GRIIDC Account password will expire soon';
        my $dateTemplate = 'at %l:%M %p %Z';
        my $pwdExpireDays = sprintf('%.0f',($pwdExpireTime - time) / 86400);
        next unless $pwdExpireDays ~~ @warn_days;
        if ($pwdExpireDays == 0) {
            $pwdExpireDays = 'today';
        }
        elsif ($pwdExpireDays == 1) {
            $pwdExpireDays = 'tomorrow';
        }
        else {
            $pwdExpireDays = "in $pwdExpireDays days";
            $dateTemplate = 'on %a, %b %d, %Y ' . $dateTemplate;
        }
        my @lt = localtime($pwdExpireTime);
        my $pwdExpireDate = strftime($dateTemplate,@lt);
        $pwdExpireDate =~ s/  / /g;

        $msg_text = <<"EOT";
Dear $name,

In order to comply with rules and procedures of Texas A&M University-Corpus Christi, our home institution, your GRIIDC Account password will expire $pwdExpireDays $pwdExpireDate.

GRIIDC Username: $uid
Associated Email Address: $mail

To change your password, please visit: https://data.gulfresearchinitiative.org/auth/cas?dest=account/password/change

$boilerplate_text
EOT
        $msg_html = <<"EOT";
<p>Dear $name,</p>
<p>In order to comply with rules and procedures of Texas A&amp;M University-Corpus Christi, our home institution, your GRIIDC Account password will expire $pwdExpireDays $pwdExpireDate.</p>
<p>GRIIDC Username: $uid<br>Associated Email Address: $mail</p>
<p>To change your password, please visit: <a href="https://data.gulfresearchinitiative.org/auth/cas?dest=account/password/change">https://data.gulfresearchinitiative.org/auth/cas?dest=account/password/change</a></p>
$boilerplate_html
EOT
        $logmsg = "$tstamp \"$name\" <$mail> was sent a message informing them that their password will expire $pwdExpireDays $pwdExpireDate.\n";
    }
    if (defined $logmsg) {
        chomp($msg_text); chomp($msg_text);
        chomp($msg_html); chomp($msg_html);
        # override email address if to_address is specified on the command line
        $mail = $to_address if defined $to_address;
        push @mail, {
                  to => "\"$name\" <$mail>",
             subject => $subject,
            msg_text => $msg_text,
            msg_html => $msg_html,
              logmsg => $logmsg
        }
    }
}
 
$mesg = $ldap->unbind;

for my $m (@mail) {
        # set up email
        my $mailer = MIME::Lite->new(
                "From" => 'GRIIDC <griidc@gomri.org>',
                  "To" => $m->{'to'},
            "Reply-To" => 'griidc@gomri.org',
             "Subject" => $m->{'subject'},
                "Type" => 'multipart/alternative'
        );
        # attach plain-text email
        $mailer->attach(
            Type => 'text/plain',
            Data => $m->{'msg_text'}
        );
        # attach HTML email
        $mailer->attach(
            Type => 'multipart/related'
        )->attach(
            Type => 'text/html',
            Data => $m->{'msg_html'}
        );
        # send email unless we're in test mode
        $mailer->send('smtp', $conf->{'mail'}->{'smtp_server'}) unless $test;
        print LOG $m->{'logmsg'} if defined $logfile;
        print $m->{'logmsg'};
        sleep($conf->{'mail'}->{'delay'});
}

close LOG if defined $logfile;
