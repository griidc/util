#/bin/sh

if [ -z $1 ]; then
    echo "You must specify offsite host."
    exit
fi

if [ $1 == "localhost" ]; then
    echo "You cannot sync to localhost."
    exit
fi

TSTAMP=`date`
echo "system sync to $1 started at $TSTAMP";

echo "Syncing /etc/logrotate.conf";
rsync -av /etc/logrotate.conf root@$1:/etc/logrotate.conf

echo "Syncing /etc/logrotate.d/";
rsync -av --delete /etc/logrotate.d/ root@$1:/etc/logrotate.d

echo "Syncing /opt/drupal/";
rsync -avA --delete /opt/drupal/ root@$1:/opt/drupal

echo "Syncing /opt/pelagos/";
rsync -av --delete /opt/pelagos/ root@$1:/opt/pelagos

echo "Syncing /etc/openldap/cacerts/cacert-triton.pem";
rsync -av --delete /etc/openldap/cacerts/cacert-triton.pem root@$1:/etc/openldap/cacerts/cacert-triton.pem

echo "Syncing /etc/opt/pelagos/";
rsync -av --delete /etc/opt/pelagos/ root@$1:/etc/opt/pelagos

echo "Syncing /var/www/";
rsync -av --delete /var/www/ root@$1:/var/www

# Subtle difference in College Station Logo
echo "Changing site logo for alternate site";
rsync -av --delete /opt/griidc/util/offsite-sync/logo-tamu.png root@$1:/opt/drupal/sites/all/themes/griidc/images/griidc-logo.png

echo "Syncing /etc/griidc/";
rsync -av --delete /etc/griidc/ root@$1:/etc/griidc

echo "Syncing /opt/griidc/";
rsync -av --delete /opt/griidc/ root@$1:/opt/griidc

echo  "Syncing /etc/griidc.ini";
rsync -av --delete /etc/griidc.ini root@$1:/etc/griidc.ini

echo  "Syncing /etc/php.ini";
rsync -av /etc/php.ini root@$1:/etc/php.ini

echo "Syncing /usr/local/griidc/";
rsync -av --delete /usr/local/griidc/ root@$1:/usr/local/griidc

echo "Syncing /etc/httpd/conf.d/";
rsync -av --delete /etc/httpd/conf.d/  root@$1:/etc/httpd/conf.d

echo "Making local changes to ssl.conf at $1"
ssh root@$1 -C "sed -i 's/^\s*ServerName.*$/ServerName poseidon-cs.tamu.edu:443/' /etc/httpd/conf.d/ssl.conf"
ssh root@$1 -C "sed -i 's/^\s*SSLCertificateFile.*$/SSLCertificateFile \/etc\/pki\/tls\/certs\/poseidon-cs_tamu_edu.pem/' /etc/httpd/conf.d/ssl.conf"
ssh root@$1 -C "sed -i 's/^\s*SSLCertificateKeyFile.*$/SSLCertificateKeyFile \/etc\/pki\/tls\/private\/poseidon-cs.tamu.edu.key/' /etc/httpd/conf.d/ssl.conf"
ssh root@$1 -C "sed -i 's/^\s*SSLCertificateChainFile.*$/SSLCertificateChainFile \/etc\/pki\/tls\/certs\/InCommon_RSA_Server_CA-chain.crt/' /etc/httpd/conf.d/ssl.conf"

echo "Syncing /var/www/piwik/";
rsync -av --delete --exclude "piwik/tmp/*" /var/www/piwik/ root@$1:/var/www/piwik

echo "Syncing /etc/httpd/ssl/";
rsync -av --delete /etc/httpd/ssl/ root@$1:/etc/httpd/ssl

echo "Syncing /usr/local/share/GRIIDC/";
rsync -av --delete /usr/local/share/GRIIDC/ root@$1:/usr/local/share/GRIIDC

echo "Syncing /usr/local/share/includes/";
rsync -av --delete /usr/local/share/includes/ root@$1:/usr/local/share/includes

echo "Syncing /usr/local/share/Slim/";
rsync -av --delete /usr/local/share/Slim/ root@$1:/usr/local/share/Slim

echo "Syncing /usr/local/share/Slim-Extras/";
rsync -av --delete /usr/local/share/Slim-Extras/ root@$1:/usr/local/share/Slim-Extras

echo "Syncing /usr/local/share/Slim-Views/";
rsync -av --delete /usr/local/share/Slim-Views/ root@$1:/usr/local/share/Slim-Views

echo "Syncing /usr/local/share/Slim2/";
rsync -av --delete /usr/local/share/Slim2/ root@$1:/usr/local/share/Slim2

echo "Syncing /usr/local/share/lightopenid-lightopenid/";
rsync -av --delete /usr/local/share/lightopenid-lightopenid/ root@$1:/usr/local/share/lightopenid-lightopenid

echo "Syncing /usr/local/share/lightopenid/";
rsync -av --delete /usr/local/share/lightopenid/ root@$1:/usr/local/share/lightopenid

echo "Syncing /usr/local/share/php/";
rsync -av --delete /usr/local/share/php/ root@$1:/usr/local/share/php

echo "Syncing  /var/lib/mysql-backups/";
rsync -av --delete /var/lib/mysql-backups/ root@$1:/var/lib/mysql-backups

echo "Syncing /etc/login.defs";
/usr/bin/rsync -av /etc/login.defs root@$1:/etc/login.defs

echo "Syncing /etc/profile.d";
/usr/bin/rsync -av /etc/profile.d/ root@$1:/etc/profile.d

echo "Setting misc ACLs";
/usr/bin/ssh root@$1 -C "setfacl -m apache:r-- /opt/pelagos/web/applications/stats/config.php"
/usr/bin/ssh root@$1 -C "setfacl -m apache:r-- /opt/pelagos/web/applications/account/config.php"
/usr/bin/ssh root@$1 -C "setfacl -m apache:r-- /opt/pelagos/web/modules/map/config.php"
/usr/bin/ssh root@$1 -C "setfacl -m apache:r-- /opt/pelagos/web/applications/doi-request/dbConfig.php"
/usr/bin/ssh root@$1 -C "setfacl -m apache:r-- /opt/pelagos/web/applications/doi-request/doiConfig.php"
/usr/bin/ssh root@$1 -C "setfacl -m apache:r-- /opt/pelagos/web/services/RIS/dbMyConfig.php"
/usr/bin/ssh root@$1 -C "setfacl -m apache:r-- /etc/opt/pelagos/db.ini"
/usr/bin/ssh root@$1 -C "setfacl -m apache:r-- /opt/pelagos/web/modules/auth/config.ini"
/usr/bin/ssh root@$1 -C "setfacl -R -m apache:rwx /opt/pelagos/var/cache/drupal_prod"
/usr/bin/ssh root@$1 -C "setfacl -m apache:rwx /opt/pelagos/var/logs"
/usr/bin/ssh root@$1 -C "setfacl -m apache:rw- /opt/pelagos/var/logs/*.log"

# restart remote webserver
echo "Restarting remote webserver $1";
/usr/bin/ssh root@$1 -C "/sbin/service httpd restart"

TSTAMP=`date`
echo "system sync to $1 ended at $TSTAMP
--------------------------------------------------------------------------------


";
