#!/bin/sh
################################################################################
# MySQL Simple Clone
# Williamson - March 2014
#
# dumps local mysql database and restores the dump on a remote server.
#
# requires: 
# mysql (clent)
# pbzip2
# remote: load_mysqldump.sh 
#
# Notes:  THIS IS NOT THREAD SAFE!
#
################################################################################
# Configuration
################################################################################
remote_user=root
remote_host=poseidon-cs.tamu.edu
export MYSQL_PWD=""
################################################################################

/bin/rm -f /var/tmp/mysql.dump; 
/usr/bin/mysqldump -u root --all-databases --events > /var/tmp/mysql.dump; 
/usr/bin/pbzip2 -f /var/tmp/mysql.dump; 
/usr/bin/scp /var/tmp/mysql.dump.bz2 $remote_user@$remote_host:/var/tmp/mysql.dump.bz2; 
/usr/bin/ssh $remote_user@$remote_host -C "/bin/chmod 600 /var/tmp/mysql.dump.bz2"; 
/usr/bin/ssh $remote_user@$remote_host -C "/usr/local/bin/load_mysqldump.sh";
