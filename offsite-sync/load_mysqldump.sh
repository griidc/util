#!/bin/sh
################################################################################
# MySQL Simple Clone load script - client (remote) script
# Williamson - March 2014
#
# restores database from mysqldump
#
# requires: 
# mysql (clent)
# pbunzip2
#
# Notes:  THIS IS NOT THREAD SAFE!
#
################################################################################
# Configuration
export MYSQL_PWD=""
logfile=/var/log/griidc/MySQL-cloning.log
################################################################################

tstamp=`date`
/usr/bin/pbunzip2 -f /var/tmp/mysql.dump.bz2
/usr/bin/mysql -u root < /var/tmp/mysql.dump
rm -f /var/tmp/mysql.dump
echo "$tstamp - loaded MySQL Dump from poseidon" >> $logfile;
