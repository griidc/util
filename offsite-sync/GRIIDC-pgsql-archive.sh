#!/bin/sh

HOSTNAME=$(hostname -s)
BU_SERVER=poseidon-cs.tamu.edu

DFILE=$1
DPATHFILE=$2

if [ ! -f "/home/data.backup/archive/$DFILE" ]
then
    cp "$DPATHFILE" "/home/data.backup/archive/$DFILE"
fi
echo "Copied $DPATHFILE to /home/data.backup/archive/$DFILE" >> /var/log/griidc/GRIIDC-pgsql-archive.log
rsync -a $DPATHFILE barman@$BU_SERVER:/var/lib/pgdata/barman/$HOSTNAME/incoming/$DFILE
echo "Copied $DPATHFILE to rsync://barman@$BU_SERVER:/var/lib/pgdata/barman/$HOSTNAME/incoming/$DFILE" >> /var/log/griidc/GRIIDC-pgsql-archive.log

