#!/usr/bin/perl

use Fcntl qw(:flock);
my $tstamp = `date`;

# Put timestamp of last rsync on filesystem
`echo "$tstamp" > /san/data/store/last-rsync.txt`;

# Establish the application lock.
open(SELF,"<",$0) || die "Cannot open $0 - $!";
flock(SELF, LOCK_EX|LOCK_NB) || die "($tstamp) $0 Already Running.";

#my $output = `/usr/local/bin/udr -a 9000 -b 9010 -c /usr/local/bin/udr rsync -av /san/data/store/ custodian\@poseidon-cs.tamu.edu:/san/data/store`;
my $output = `rsync -av /san/data/store/ custodian\@poseidon-cs.tamu.edu:/san/data/store`;

print "($tstamp) $output\n";
