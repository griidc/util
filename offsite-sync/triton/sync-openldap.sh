#!/bin/sh
date=$(date +%Y%m%d%H%M%S)

# stop slapd and delete all config/data
/usr/bin/ssh root@triton-cs.tamu.edu -C "service slapd stop"
/usr/bin/rsync -av --delete /etc/openldap/ root@triton-cs.tamu.edu:/etc/openldap
/usr/bin/ssh root@triton-cs.tamu.edu -C "rm -rf /etc/openldap/slapd.d/*"
/usr/bin/ssh root@triton-cs.tamu.edu -C "rm -rf /var/lib/ldap/*"
# reown as ldap.ldap 
/usr/bin/ssh root@triton-cs.tamu.edu -C "chown -R ldap.ldap /var/lib/ldap"
# locally generate slapcat dumps of config and data, transfer to DR site.
/usr/sbin/slapcat -F /etc/openldap/slapd.d -n0 -l /var/tmp/openldap-db0-dump.$$.$date.ldif
/usr/sbin/slapcat -n2 -l /var/tmp/openldap-db2-dump.$$.$date.ldif
/usr/bin/rsync -av /var/tmp/openldap-db?-dump.$$.$date.ldif root@triton-cs.tamu.edu:/opt/openldap-backup
/bin/rm -f /var/tmplopenldap-db?-dump.$$.$date.ldif
# load dumps, reown directories as ldap.ldap, start
/usr/bin/ssh root@triton-cs.tamu.edu -C "slapadd -F /etc/openldap/slapd.d -n0 -l /opt/openldap-backup/openldap-db0-dump.$$.$date.ldif"
/usr/bin/ssh root@triton-cs.tamu.edu -C "slapadd -n2 -l /opt/openldap-backup/openldap-db2-dump.$$.$date.ldif"
/usr/bin/ssh root@triton-cs.tamu.edu -C "chown -R ldap.ldap /var/lib/ldap/"
/usr/bin/ssh root@triton-cs.tamu.edu -C "chown -R ldap.ldap /etc/openldap/slapd.d/"
/usr/bin/ssh root@triton-cs.tamu.edu -C "service slapd start"
# prune any backups older than 30 days
/usr/bin/ssh root@triton-cs.tamu.edu -C "/bin/find /opt/openldap-backup/ -mtime +30 -exec rm -f {} \;"
