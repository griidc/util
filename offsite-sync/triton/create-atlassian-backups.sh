#!/bin/sh
# Script to make consistent backups of Atlassian software.
#
# Atlassian applications must be shut down at the time this script is run!

# Number of day to keep these backups on disk. This directory should be backed up on tape as well,
# but in the event of backup tape job failure, this increases the the opportunity to make it to tape.
keep=3

date=`/bin/date "+%Y-%m-%d-%H-%M-%S"`

# Shut down PRODUCTION Tomcat apps so we can have a consistent source.
for myservice in jira atlbitbucket atlbitbucket_search
do
    /sbin/service $myservice stop
done;

# Create database backups
export PGPASSFILE=/root/.pgpass
/usr/bin/pg_dump -U postgres jira -c > "/home/atlassian-backups/jira/jira-db_$date"".sql"
/usr/bin/pg_dump -U postgres stash -c > "/home/atlassian-backups/bitbucket/bitbucket-db_$date"".sql"

# Create application backups.
/bin/tar cf "/home/atlassian-backups/jira/jira_$date"".tar" /opt/atlassian/jira /var/atlassian/application-data/jira /etc/init.d/jira
/bin/tar cf "/home/atlassian-backups/bitbucket/bitbucket_$date"".tar" /opt/atlassian/bitbucket /var/atlassian/application-data/stash /etc/init.d/atlbitbucket /etc/init.d/atlbitbucket_search

# Restart Atlassian Applications
for myservice in jira atlbitbucket atlbitbucket_search
do
    /sbin/service $myservice start
done;

# Compress backups just made
/usr/bin/pbzip2 "/home/atlassian-backups/jira/jira-db_$date"".sql"
/usr/bin/pbzip2 "/home/atlassian-backups/bitbucket/bitbucket-db_$date"".sql"
/usr/bin/pbzip2 "/home/atlassian-backups/jira/jira_$date"".tar"
/usr/bin/pbzip2 "/home/atlassian-backups/bitbucket/bitbucket_$date"".tar"

# Prune old backups
/bin/find /home/atlassian-backups -type f -mtime +$keep -exec rm -f {} \;
