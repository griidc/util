#!/bin/sh

export archive_dir=/mnt/atlassian-backups
export application=jira
export app_db_name=jira

# shut down application
/sbin/service $application stop

# remove application bin directory, working directory, old tempdir
rm -rf /opt/atlassian/$application
rm -rf /var/atlassian/application-data/$application
rm -rf /var/tmp/$application-restore

# locate latest backup - only look for files created within 24 hours
backup_archive=$(find /mnt/atlassian-backups/$application -mtime -1 -type f -name "${application}_*" | sort -r | head -n 1)
backup_database=$(find /mnt/atlassian-backups/$application -mtime -1 -type f -name "$application-db*" | sort -r | head -n 1)
backup_archive=$(echo $backup_archive | sed "s/\/mnt\/atlassian-backups\/$application\///g")
backup_database=$(echo $backup_database | sed "s/\/mnt\/atlassian-backups\/$application\///g")

[[ -z "$backup_archive" ]] && { echo "Could not find filesystem backup archive. Stopping." ; exit 1; }
[[ -z "$backup_database" ]] && { echo "Could not find database backup. Stopping." ; exit 1; }

# unpack into tempdir
mkdir /var/tmp/$application-restore
/bin/tar xfvj $archive_dir/$application/$backup_archive -C /var/tmp/$application-restore
/bin/cp $archive_dir/$application/$backup_database  /var/tmp/$application-restore

# move bindir into place
/bin/mv /var/tmp/$application-restore/opt/atlassian/$application /opt/atlassian

# move workdir into place
/bin/mv /var/tmp/$application-restore/var/atlassian/application-data/$application /var/atlassian/application-data

# move init.d script into place
/bin/mv /var/tmp/$application-restore/etc/init.d/$application /etc/init.d/$application

# drop database
/bin/echo "DROP DATABASE $app_db_name;" | /usr/bin/psql -U postgres

# create database
/bin/echo "CREATE DATABASE $app_db_name;" | /usr/bin/psql -U postgres

# populate database
/usr/bin/bunzip2 /var/tmp/$application-restore/$backup_database
export backup_uncompressed=$(echo $backup_database | sed 's/\.bz2$//')
/usr/bin/psql -U postgres $app_db_name < /var/tmp/$application-restore/$backup_uncompressed

# run post-clone scripts
echo "UPDATE cwd_directory_attribute SET attribute_value = 'ldap://triton-cs.tamu.edu:389' WHERE attribute_name = 'ldap.url' AND directory_id = 10000;" | psql -U postgres jira;

# remove old tempfiles
rm -rf /var/tmp/$application-restore

# start up application
/sbin/service $application start

# Change baseUrl via API call
sleep 300
resp=$(curl -v -u mwilliamson:Up-Equally-Honestly-Father -X PUT -H 'Content-Type:application/json' https://triton-cs.tamu.edu/issues/rest/api/2/settings/baseUrl -d 'https://triton-cs.tamu.edu/issues')
