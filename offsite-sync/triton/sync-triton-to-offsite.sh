#!/bin/sh
primarySite=triton.tamucc.edu
secondarySite=triton-cs.tamu.edu

# GENERIC WEB ##################################################################
# apache / web root
/usr/bin/rsync -av --delete /var/www/ root@$secondarySite:/var/www
# php parameters/settings
/usr/bin/rsync -av  /etc/php.ini root@$secondarySite:/etc/php.ini
/usr/bin/rsync -av --delete /etc/php.d/ root@$secondarySite:/etc/php.d
# general config
/usr/bin/rsync -av /etc/httpd/conf/httpd.conf root@$secondarySite:/etc/httpd/conf/httpd.conf
/usr/bin/rsync -av /etc/httpd/conf.d/ root@$secondarySite:/etc/httpd/conf.d

# apply customizations to local $secondarySite apache config.
/usr/bin/ssh -C root@$secondarySite -C "sed -i 's/triton.tamucc.edu/triton-cs.tamu.edu/' /etc/httpd/conf/httpd.conf"
/usr/bin/ssh -C root@$secondarySite -C "sed -i 's/^\s*SSLCertificateFile.*$/SSLCertificateFile \/etc\/pki\/tls\/certs\/triton-cs_tamu_edu.pem/' /etc/httpd/conf.d/ssl.conf"
/usr/bin/ssh -C root@$secondarySite -C "sed -i 's/^\s*SSLCertificateKeyFile.*$/SSLCertificateKeyFile \/etc\/pki\/tls\/private\/triton-cs.tamu.edu.key/' /etc/httpd/conf.d/ssl.conf"
/usr/bin/ssh -C root@$secondarySite -C "sed -i 's/^\s*SSLCertificateChainFile.*$/SSLCertificateChainFile \/etc\/httpd\/ssl\/InCommon_RSA_Server_CA-chain.crt/' /etc/httpd/conf.d/ssl.conf"

# control panels
/usr/bin/rsync -av --delete /etc/phpldapadmin/ root@$secondarySite:/etc/phpldapadmin
/usr/bin/rsync -av --delete /etc/phpPgAdmin/ root@$secondarySite:/etc/phpPgAdmin
# ssl certificates
/usr/bin/rsync -av --delete /etc/pki/CA/ root@$secondarySite:/etc/pki/CA
/usr/bin/rsync -av --delete /etc/pki/tls/ root@$secondarySite:/etc/pki/tls
#-------------------------------------------------------------------------------

# logrotate configuration
/usr/bin/rsync -av /etc/logrotate.conf root@$secondarySite:/etc/logrotate.conf
/usr/bin/rsync -av --delete /etc/logrotate.d/ root@$secondarySite:/etc/logrotate.d

# DRUPAL #######################################################################
/usr/bin/rsync -av --delete /opt/drupal/ root@$secondarySite:/opt/drupal
/usr/bin/rsync -av /etc/httpd/conf.d/drupal.conf root@$secondarySite:/etc/httpd/conf.d/drupal.conf
/usr/bin/ssh root@$secondarySite -C "/usr/bin/rpl -i $primarySite $secondarySite /etc/httpd/conf.d/drupal.conf"
/usr/bin/ssh root@$secondarySite -C "/sbin/service httpd restart"
#-------------------------------------------------------------------------------

# GENERIC TOMCAT ###############################################################
/usr/bin/rsync -av --delete /etc/tomcat6/ root@$secondarySite:/etc/tomcat6
/usr/bin/rsync -av --delete /var/cache/tomcat6/ root@$secondarySite:/var/cache/tomcat6
/usr/bin/rsync -av --delete /var/log/tomcat6/ root@$secondarySite:/var/log/tomcat6
#-------------------------------------------------------------------------------

# login rules per TAMUCC policy ################################################
/usr/bin/rsync -av --delete /etc/login.defs root@$secondarySite:/etc/login.defs
/usr/bin/rsync -av --delete /etc/pam.d/system-auth root@$secondarySite:/etc/pam.d/system-auth
#-------------------------------------------------------------------------------

# NOTICE #######################################################################
# THESE ITEMS REQUIRE MANUAL EDITING ON THE OFFSITE SITE. Do not rsync these!
# /etc/httpd/ssl
#-------------------------------------------------------------------------------
