#!/bin/sh

# This script load FCOE data into RIS

source /opt/pelagos/share/bash/db-utils.lib.sh

# pull in database configuration parameters
get_db_params /etc/opt/pelagos/db-admin.ini RIS_LOCAL_ROOT THIS_

CLIENT_PRG="/usr/bin/mysql"

$CLIENT_PRG -u $THIS_username -p$THIS_password $THIS_dbname -e \
"
-- Load Institutions not already in RIS (start IDs at 1000)

-- * none at this time *

-- Delete FL-COE People not already in RIS

DELETE FROM People WHERE People_ID > 9000 AND People_ID < 9050;

-- Load FL-COE People not already in RIS (starting IDs at 9001)

INSERT INTO People ( People_ID, People_FirstName, People_LastName,    People_Email,                  People_Institution ) VALUES
                   ( 9001,      'David',          'Chagaris',         'dchagaris@ufl.edu',           16                 ),
                   ( 9002,      'Mike',           'Allen',            'msal@ufl.edu',                16                 ),
                   ( 9003,      'Steven',         'Smith',            'steve.smith@rsmas.miami.edu', 36                 ),
                   ( 9004,      'Jerald',         'Ault',             'jault@rsmas.miami.edu',       36                 ),
                   ( 9005,      'Arnaud',         'Gruss',            'a.gruss@miami.edu',           36                 ),
                   ( 9006,      'Elizabeth',      'Babcock',          'ebabcock@rsmas.miami.edu',    36                 ),
                   ( 9007,      'Jane M.',        'Caffrey',          'jcaffrey@uwf.edu',            35                 ),
                   ( 9008,      'Erin',           'Seney',            'erin.seney@ucf.edu',          40                 ),
                   ( 9009,      'Katherine',      'Mansfield',        'kate.mansfield@ucf.edu',      40                 ),
                   ( 9010,      'Mya',            'Breitbart',        'mya@usf.edu',                 23                 ),
                   ( 9011,      'Sean',           'Keenan',           'sean.keenan@myfwc.com',       42                 ),
                   ( 9012,      'Brian',          'Walker',           'walkerb@nova.edu',            34                 ),
                   ( 9013,      'Elizabeth',      'Fetherston-Resch', 'ehfetherston@usf.edu',        79                 );


-- Delete FL-COE Associations

DELETE FROM ProjPeople WHERE Program_ID > 700 AND Program_ID < 750;

-- Load FL-COE Associations (starting IDs at 1000001)

INSERT INTO ProjPeople ( ProjPeople_ID, Program_ID, Project_ID, People_ID, Role_ID ) VALUES
                       ( 1000001,       701,        0,          9001,      2       ),
                       ( 1000002,       701,        0,          9002,      1       ),
                       ( 1000003,       702,        0,          9003,      2       ),
                       ( 1000004,       702,        0,          9004,      1       ),
                       ( 1000005,       703,        0,          9005,      2       ),
                       ( 1000006,       703,        0,          9006,      1       ),
                       ( 1000007,       704,        0,          1827,      2       ),
                       ( 1000008,       704,        0,          3227,      1       ),
                       ( 1000009,       705,        0,          1827,      2       ),
                       ( 1000010,       705,        0,          9007,      1       ),
                       ( 1000011,       706,        0,          2709,      2       ),
                       ( 1000012,       706,        0,          112,       1       ),
                       ( 1000013,       707,        0,          405,       2       ),
                       ( 1000014,       707,        0,          3426,      1       ),
                       ( 1000015,       708,        0,          9008,      2       ),
                       ( 1000016,       708,        0,          9009,      1       ),
                       ( 1000017,       709,        0,          9010,      2       ),
                       ( 1000018,       709,        0,          769,       1       ),
                       ( 1000019,       710,        0,          9011,      2       ),
                       ( 1000020,       710,        0,          9012,      1       ),
                       ( 1000021,       701,        0,          9013,      3       ),
                       ( 1000022,       702,        0,          9013,      3       ),
                       ( 1000023,       703,        0,          9013,      3       ),
                       ( 1000024,       704,        0,          9013,      3       ),
                       ( 1000025,       705,        0,          9013,      3       ),
                       ( 1000026,       706,        0,          9013,      3       ),
                       ( 1000027,       707,        0,          9013,      3       ),
                       ( 1000028,       708,        0,          9013,      3       ),
                       ( 1000029,       709,        0,          9013,      3       ),
                       ( 1000030,       710,        0,          9013,      3       );
"

exit 0
