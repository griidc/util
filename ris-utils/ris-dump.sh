#!/bin/sh

# This script pulls a dump of the main GoMRI database and pipes it to the mysql
# client, to duplicate that DB locally. It then compensates for a couple of
# problematic characters in one name by updating that particular record with
# the correct implementation of his surname.

# To enter the correct characters for Tamay's name, <Ctrl-V>214 and <Ctrl-V>246

source /opt/pelagos/share/bash/db-utils.lib.sh

# pull in database configuration parameters
get_db_params /etc/opt/pelagos/db-admin.ini RIS_REMOTE_RO MASTER_
get_db_params /etc/opt/pelagos/db-admin.ini RIS_LOCAL_ROOT THIS_

###################################
# Script "CONSTANTS"              #
###################################
CLIENT_PRG="/usr/bin/mysql"
DUMP_PRG="/usr/bin/mysqldump --opt"

###################################
# Script Exit Statuses            #
###################################
E_BAD_CLEANUP=65
E_BAD_DUMP=66
E_BAD_INSERT=67
SUCCESS=0

###################################
# Script variables:"              #
###################################
dump_file=/var/lib/mysql/gomri_rpis/gomri_dump.$RANDOM
exit_status=$SUCCESS

# Bluehost seems to have implemented some sort of greylisting where an initial
# connection is refused, but a subsequent one is accepted. Let's see if this
# helps:
telnet $MASTER_host $MASTER_port > /dev/null 2>&1

# First, dump the master DB to the dump file:
$DUMP_PRG -h $MASTER_host -u $MASTER_username -p$MASTER_password $MASTER_dbname > $dump_file
if [ $? -ne 0 ]
then
   echo "Something went wrong with the $MASTER_dbname dump. Check the log files"
   if [ -e $dump_file ]
   then
      rm -f $dump_file || exit $E_BAD_CLEANUP
   fi
   exit $E_BAD_DUMP
fi

# Now, load the dumped file:
$CLIENT_PRG -u $THIS_username -p$THIS_password $THIS_dbname < $dump_file

# This only tests the output of the last command in the pipeline. Might
# have to think about dumping either to a temp file or to a here docuemnt,
# testing that exit status, then if successful, redirecting it to the client.
if [ $? -ne 0 ]
then
   echo "Something went wrong with the import to $THIS_dbname. Check the log files"
   exit_status=$E_BAD_INSERT
fi

# Now clean up the collation and character set mess:
if [ -x /opt/pelagos/util/ris-utils/fix_ris.sh ]
then
   /opt/pelagos/util/ris-utils/fix_ris.sh
fi

# Clean up:
if [ -e $dump_file ]
then
   rm -f $dump_file || exit $E_BAD_CLEANUP
fi
