#!/bin/sh

# This script load FCOE data into RIS

source /opt/pelagos/share/bash/db-utils.lib.sh

# pull in database configuration parameters
get_db_params /etc/opt/pelagos/db-admin.ini RIS_LOCAL_ROOT THIS_

CLIENT_PRG="/usr/bin/mysql"

$CLIENT_PRG -u $THIS_username -p$THIS_password $THIS_dbname -e \
"
-- Load FL-COE Funding Cycle #1

INSERT INTO FundingSource (Fund_ID, Fund_Source, Fund_Name, Fund_Sort)
    VALUES (700, 'FLRACEP', 'Florida RESTORE Act Centers of Excellence Program', 700);

-- Load Projects for FL-COE Funding Cycle #1

INSERT INTO Programs (Program_ID, Program_Title, Program_FundSrc)
    VALUES (701, 'Examining Fisheries Impact of Invasive Lionfish with an Ecopath with Ecosim Model', 700);
INSERT INTO Programs (Program_ID, Program_Title, Program_FundSrc)
    VALUES (702, 'Biological and Economic Indicators for Assessing Recreational Fisheries', 700);
INSERT INTO Programs (Program_ID, Program_Title, Program_FundSrc)
    VALUES (703, 'Improving the use of products derived from monitoring data in ecosystem models of the Gulf of Mexico', 700);
INSERT INTO Programs (Program_ID, Program_Title, Program_FundSrc)
    VALUES (704, 'Fishery-Independent Surveys of Reef Fish Community, Size, and Age Structure off Northwest Florida', 700);
INSERT INTO Programs (Program_ID, Program_Title, Program_FundSrc)
    VALUES (705, 'Evaluating Fish Production and Ecosystem Impacts of Artificial Reefs', 700);
INSERT INTO Programs (Program_ID, Program_Title, Program_FundSrc)
    VALUES (706, 'Monitoring oil spill effects and recovery in large deep-sea fishes', 700);
INSERT INTO Programs (Program_ID, Program_Title, Program_FundSrc)
    VALUES (707, 'Demonstration of Fisheries Assessment Applications for Underwater Gliders', 700);
INSERT INTO Programs (Program_ID, Program_Title, Program_FundSrc)
    VALUES (708, 'Ontogenetic Shifts in Sea Turtle Habitat Use and Foraging Ecology', 700);
INSERT INTO Programs (Program_ID, Program_Title, Program_FundSrc)
    VALUES (709, 'Egg and larval barcoding for Gulf DEPM stock assessments', 700);
INSERT INTO Programs (Program_ID, Program_Title, Program_FundSrc)
    VALUES (710, 'Hardbottom Mapping and Community Characterization of the West-Central Florida Gulf Coast', 700);
"

exit 0
