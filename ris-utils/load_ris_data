#!/bin/sh

# This script loads data into RIS
# It expects one parameter: an SQL file INSERT statements for the data to load

###################################
# Script "CONSTANTS":             #
###################################
CLIENT_PRG="/usr/bin/mysql"

###################################
# Script Exit Statuses:           #
###################################
E_BAD_INSERT=67
E_NO_DATA_FILE_SPECIFIED=68
E_DATA_FILE_NOT_FOUND=69
SUCCESS=0

###################################
# Script variables:"              #
###################################
exit_status=$SUCCESS

if [ -z $1 ]
then
    echo "You must specify a data file to load."
    exit $E_NO_DATA_FILE_SPECIFIED
fi

if [ ! -f $1 ]
then
    echo "Data file not found."
    exit $E_DATA_FILE_NOT_FOUND
fi

# import bash db-utils library
source /opt/pelagos/share/bash/db-utils.lib.sh

# pull in database configuration parameters
get_db_params /etc/opt/pelagos/db-admin.ini RIS_LOCAL_ROOT RIS_

# Import test data
$CLIENT_PRG -u $RIS_username -p$RIS_password $RIS_dbname < $1

# Let's see if that worked:
if [ $? -ne 0 ]
then
    echo "Something went wrong with importing test data. Check the log files"
    exit_status=$E_BAD_INSERT
fi

exit $exit_status
