#!/bin/sh

# This script pulls a dump of the main GoMRI database and pipes it to the mysql
# client, to duplicate that DB locally. It then compensates for a couple of
# problematic characters in one name by updating that particular record with
# the correct implementation of his surname.

# To enter the correct characters for Tamay's name, <Ctrl-V>214 and <Ctrl-V>246

source /opt/pelagos/share/bash/db-utils.lib.sh

# pull in database configuration parameters
get_db_params /etc/opt/pelagos/db-admin.ini RIS_REMOTE_RO MASTER_
get_db_params /etc/opt/pelagos/db-admin.ini RIS_LOCAL_ROOT THIS_

###################################
# Script "CONSTANTS"              #
###################################
CLIENT_PRG="/usr/bin/mysql"
DUMP_PRG="/usr/bin/mysqldump --opt --default-character-set=latin1"

###################################
# Script Exit Statuses            #
###################################
E_BAD_CLEANUP=65
E_BAD_DUMP=66
E_BAD_INSERT=67
SUCCESS=0

###################################
# Script variables:"              #
###################################
dump_file=/var/lib/mysql/gomri_rpis/gomri_dump.$RANDOM
exit_status=$SUCCESS


# Now, load the dumped file:
$CLIENT_PRG -u $THIS_username -p$THIS_password $THIS_dbname -e \
" -- Departments table modifications:
 ALTER TABLE Departments
    MODIFY Department_Name BLOB,
    MODIFY Department_Addr1 BLOB,
    MODIFY Department_Addr2 BLOB,
    MODIFY Department_City BLOB,
    MODIFY Department_State BLOB,
    MODIFY Department_Zip BLOB,
    MODIFY Department_Country BLOB,
    MODIFY Department_URL BLOB;

 ALTER TABLE Departments
    MODIFY Department_Name TEXT CHARSET utf8,
    MODIFY Department_Addr1 TEXT CHARSET utf8,
    MODIFY Department_Addr2 TEXT CHARSET utf8,
    MODIFY Department_City TEXT CHARSET utf8,
    MODIFY Department_State TEXT CHARSET utf8,
    MODIFY Department_Zip TEXT CHARSET utf8,
    MODIFY Department_Country TEXT CHARSET utf8,
    MODIFY Department_URL TEXT CHARSET utf8;


 -- FuncingSource table modifications:
 ALTER TABLE FundingSource
    MODIFY Fund_Source BLOB,
    MODIFY Fund_Link BLOB,
    MODIFY Fund_Name BLOB;

 ALTER TABLE FundingSource
    MODIFY Fund_Source TEXT CHARSET utf8,
    MODIFY Fund_Link TEXT CHARSET utf8,
    MODIFY Fund_Name TEXT CHARSET utf8;


 -- Institutions table modifications:
 DROP INDEX Institution_Name
    ON Institutions;
 ALTER TABLE Institutions
    MODIFY Institution_Name BLOB,
    MODIFY Institution_Addr1 BLOB,
    MODIFY Institution_Addr2 BLOB,
    MODIFY Institution_City BLOB,
    MODIFY Institution_State BLOB,
    MODIFY Institution_Zip BLOB,
    MODIFY Institution_Country BLOB,
    MODIFY Institution_URL BLOB,
    MODIFY Institution_Lat BLOB,
    MODIFY Institution_Long BLOB,
    MODIFY Institution_Keywords BLOB;

 ALTER TABLE Institutions
    MODIFY Institution_Name TEXT CHARSET utf8,
    MODIFY Institution_Addr1 TEXT CHARSET utf8,
    MODIFY Institution_Addr2 TEXT CHARSET utf8,
    MODIFY Institution_City TEXT CHARSET utf8,
    MODIFY Institution_State TEXT CHARSET utf8,
    MODIFY Institution_Zip TEXT CHARSET utf8,
    MODIFY Institution_Country TEXT CHARSET utf8,
    MODIFY Institution_URL TEXT CHARSET utf8,
    MODIFY Institution_Lat TEXT CHARSET utf8,
    MODIFY Institution_Long TEXT CHARSET utf8,
    MODIFY Institution_Keywords TEXT CHARSET utf8;


 -- People table modifications:
 ALTER TABLE People
    MODIFY People_Title BLOB,
    MODIFY People_LastName BLOB,
    MODIFY People_FirstName BLOB,
    MODIFY People_MiddleName BLOB,
    MODIFY People_Suffix BLOB,
    MODIFY People_AdrStreet1 BLOB,
    MODIFY People_AdrStreet2 BLOB,
    MODIFY People_AdrCity BLOB,
    MODIFY People_AdrState BLOB,
    MODIFY People_AdrZip BLOB,
    MODIFY People_Email BLOB,
    MODIFY People_PhoneNum BLOB,
    MODIFY People_GulfBase BLOB;

 ALTER TABLE People
    MODIFY People_Title TEXT CHARSET utf8,
    MODIFY People_LastName TEXT CHARSET utf8,
    MODIFY People_FirstName TEXT CHARSET utf8,
    MODIFY People_MiddleName TEXT CHARSET utf8,
    MODIFY People_Suffix TEXT CHARSET utf8,
    MODIFY People_AdrStreet1 TEXT CHARSET utf8,
    MODIFY People_AdrStreet2 TEXT CHARSET utf8,
    MODIFY People_AdrCity TEXT CHARSET utf8,
    MODIFY People_AdrState TEXT CHARSET utf8,
    MODIFY People_AdrZip TEXT CHARSET utf8,
    MODIFY People_Email TEXT CHARSET utf8,
    MODIFY People_PhoneNum TEXT CHARSET utf8,
    MODIFY People_GulfBase TEXT CHARSET utf8;


 -- Programs table modifications:
 DROP INDEX TxtSearch
    ON Programs;
 ALTER TABLE Programs
    MODIFY Program_Title BLOB,
    MODIFY Program_StartDate BLOB,
    MODIFY Program_EndDate BLOB,
    MODIFY Program_ExtDate BLOB,
    MODIFY Program_Goals BLOB,
    MODIFY Program_Purpose BLOB,
    MODIFY Program_Objective BLOB,
    MODIFY Program_Abstract BLOB,
    MODIFY Program_Location BLOB,
    MODIFY Program_WebAddr BLOB,
    MODIFY Program_SGLink BLOB,
    MODIFY Program_Comment BLOB;

 ALTER TABLE Programs
    MODIFY Program_Title TEXT CHARSET utf8,
    MODIFY Program_StartDate TEXT CHARSET utf8,
    MODIFY Program_EndDate TEXT CHARSET utf8,
    MODIFY Program_ExtDate TEXT CHARSET utf8,
    MODIFY Program_Goals TEXT CHARSET utf8,
    MODIFY Program_Purpose TEXT CHARSET utf8,
    MODIFY Program_Objective TEXT CHARSET utf8,
    MODIFY Program_Abstract TEXT CHARSET utf8,
    MODIFY Program_Location TEXT CHARSET utf8,
    MODIFY Program_WebAddr TEXT CHARSET utf8,
    MODIFY Program_SGLink TEXT CHARSET utf8,
    MODIFY Program_Comment TEXT CHARSET utf8;


 -- Projects table modifications:
 ALTER TABLE Projects
    MODIFY Project_Title BLOB,
    MODIFY Project_Goals BLOB,
    MODIFY Project_Purpose BLOB,
    MODIFY Project_Objective BLOB,
    MODIFY Project_Abstract BLOB,
    MODIFY Project_WebAddr BLOB,
    MODIFY Project_Location BLOB,
    MODIFY Project_SGLink BLOB,
    MODIFY Project_Comment BLOB;

 ALTER TABLE Projects
    MODIFY Project_Title TEXT CHARSET utf8,
    MODIFY Project_Goals TEXT CHARSET utf8,
    MODIFY Project_Purpose TEXT CHARSET utf8,
    MODIFY Project_Objective TEXT CHARSET utf8,
    MODIFY Project_Abstract TEXT CHARSET utf8,
    MODIFY Project_WebAddr TEXT CHARSET utf8,
    MODIFY Project_Location TEXT CHARSET utf8,
    MODIFY Project_SGLink TEXT CHARSET utf8,
    MODIFY Project_Comment TEXT CHARSET utf8;


 -- Roles table modifications:
 ALTER TABLE Roles
    MODIFY Role_Name BLOB;

 ALTER TABLE Roles
    MODIFY Role_Name TEXT CHARSET utf8;"

exit 0
