#!/usr/bin/perl -w

use strict;
use Config::PHPINI;
use DBI;

my $db_conf = Config::PHPINI->read('/etc/opt/pelagos/db.ini');

my %dbi_type_map = ('postgresql' => 'Pg', 'mysql' => 'mysql');

my %dbSource = %{$db_conf->{'RIS_RO'}};
my %dbTarget = %{$db_conf->{'GOMRI_RW'}};

my $ris_dbh = DBI->connect("DBI:$dbi_type_map{$dbSource{type}}:host=$dbSource{host};port=$dbSource{port};dbname=$dbSource{dbname};",$dbSource{'username'},$dbSource{'password'},{'RaiseError' => 1});
my $gomri_dbh = DBI->connect("DBI:$dbi_type_map{$dbTarget{type}}:host=$dbTarget{host};port=$dbTarget{port};dbname=$dbTarget{dbname};",$dbTarget{'username'},$dbTarget{'password'},{'RaiseError' => 1, 'AutoCommit' => 0});

$ris_dbh->do("set names 'utf8'");

my $ris_sth = $ris_dbh->prepare(
   "SELECT
        Program_ID,
        Program_Title,
        Program_FundSrc,
        Program_LeadInstitution,
        Program_SubTasks,

        IF (Program_StartDate = '0000-00-00', null, Program_StartDate) as Program_StartDate,
        IF (Program_EndDate = '0000-00-00', null, Program_EndDate) as Program_EndDate,
        IF (Program_ExtDate = '0000-00-00', null, Program_ExtDate) as Program_ExtDate,
        Program_Goals,
        Program_Purpose,

        Program_Objective,
        Program_Abstract,
        Program_Location,
        Program_WebAddr,
        Program_SGLink,

        Program_SGRecID,
        Program_Comment,
        Program_Completed,
        Program_Theme1,
        Program_Theme2,

        Program_Theme3,
        Program_Theme4,
        Program_Theme5
    FROM
        Programs"
    );

$ris_sth->execute();
my $ref = $ris_sth->fetchall_hashref('Program_ID');
if ($DBI::errstr) {
    print "DB Error: $DBI::errstr\n";
    exit;
}
$ris_dbh->disconnect();

if (ref($ref) eq 'HASH') {
} else {
    print "DB Error: unexpected RIS data.";
    exit;
}

eval {
    my $gomri_sth = $gomri_dbh->prepare("TRUNCATE projects");
    $gomri_sth->execute();

    $gomri_sth = $gomri_dbh->prepare(
        'INSERT INTO projects (
            "ID",
            "Title",
            "FundSrc",
            "LeadInstitution",
            "SubTasks",

            "StartDate",
            "EndDate",
            "ExtDate",
            "Goals",
            "Purpose",

            "Objective",
            "Abstract",
            "Location",
            "WebAddr",
            "SGLink",

            "SGRecID",
            "Comment",
            "Completed",
            "Theme1",
            "Theme2",

            "Theme3",
            "Theme4",
            "Theme5"
        )
        VALUES (
            ?, ?, ?, ?, ?,
            ?, ?, ?, ?, ?,
            ?, ?, ?, ?, ?,
            ?, ?, ?, ?, ?,
            ?, ?, ?
        )'
    );

    foreach my $program (keys %{$ref}) {

        $gomri_sth->bind_param(1, $ref->{$program}->{'Program_ID'});
        $gomri_sth->bind_param(2, $ref->{$program}->{'Program_Title'});
        $gomri_sth->bind_param(3, $ref->{$program}->{'Program_FundSrc'});
        $gomri_sth->bind_param(4, $ref->{$program}->{'Program_LeadInstitution'});
        $gomri_sth->bind_param(5, $ref->{$program}->{'Program_SubTasks'});
        $gomri_sth->bind_param(6, $ref->{$program}->{'Program_StartDate'});
        $gomri_sth->bind_param(7, $ref->{$program}->{'Program_EndDate'});
        $gomri_sth->bind_param(8, $ref->{$program}->{'Program_ExtDate'});
        $gomri_sth->bind_param(9, $ref->{$program}->{'Program_Goals'});
        $gomri_sth->bind_param(10, $ref->{$program}->{'Program_Purpose'});
        $gomri_sth->bind_param(11, $ref->{$program}->{'Program_Objective'});
        $gomri_sth->bind_param(12, $ref->{$program}->{'Program_Abstract'});
        $gomri_sth->bind_param(13, $ref->{$program}->{'Program_Location'});
        $gomri_sth->bind_param(14, $ref->{$program}->{'Program_WebAddr'});
        $gomri_sth->bind_param(15, $ref->{$program}->{'Program_SGLink'});
        $gomri_sth->bind_param(16, $ref->{$program}->{'Program_SGRecID'});
        $gomri_sth->bind_param(17, $ref->{$program}->{'Program_Comment'});
        $gomri_sth->bind_param(18, $ref->{$program}->{'Program_Completed'});
        $gomri_sth->bind_param(19, $ref->{$program}->{'Program_Theme1'});
        $gomri_sth->bind_param(20, $ref->{$program}->{'Program_Theme2'});
        $gomri_sth->bind_param(21, $ref->{$program}->{'Program_Theme3'});
        $gomri_sth->bind_param(22, $ref->{$program}->{'Program_Theme4'});
        $gomri_sth->bind_param(23, $ref->{$program}->{'Program_Theme5'});

        $gomri_sth->execute();
    }

    $gomri_dbh->commit();
    $gomri_dbh->disconnect();
}
