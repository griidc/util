#!/bin/sh

# This script load FCOE data into RIS

source /opt/pelagos/share/bash/db-utils.lib.sh

# pull in database configuration parameters
get_db_params /etc/opt/pelagos/db-admin.ini RIS_LOCAL_ROOT THIS_

CLIENT_PRG="/usr/bin/mysql"

$CLIENT_PRG -u $THIS_username -p$THIS_password $THIS_dbname -e \
"
DELETE FROM ProjPeople WHERE Program_ID = 750;
DELETE FROM Programs WHERE Program_ID = 750;
DELETE FROM FundingSource WHERE Fund_ID = 750;
DELETE FROM People WHERE People_ID = 9050;
DELETE FROM Institutions WHERE Institution_ID = 1000;

INSERT INTO FundingSource ( Fund_ID, Fund_Source, Fund_Name,              Fund_Sort ) VALUES
                          ( 750,     'BP',        'BP Gulf Science Data', 750       );

INSERT INTO Programs ( Program_ID, Program_Title,          Program_FundSrc ) VALUES
                     ( 750,        'BP Gulf Science Data', 750             );

INSERT INTO Institutions ( Institution_ID, Institution_Name, Institution_Verified ) VALUES
                         ( 1000,           'BP',             1                    );

INSERT INTO People ( People_ID, People_FirstName, People_LastName,     People_Email,       People_Institution ) VALUES
                   ( 9050,      'BP',             'Gulf Science Data', 'griidc@gomri.org', 1000               );

INSERT INTO ProjPeople ( ProjPeople_ID, Program_ID, Project_ID, People_ID, Role_ID ) VALUES
                       ( 1000101,       750,        0,          9050,      1       );
"

exit 0
