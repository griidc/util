#!/bin/sh
################################################################################
# March 2014 - Williamson
#
# This tool generates a LVM snapshot of the filesystem on the e48 SAN.  It 
# also creates a mountpoint and mounts the new snapshot readonly. 
#
################################################################################
#
# Required: LVM2, adequate unallocated space on a LVM PV for snapshots, xfs 
#           is assumed, LV named /dev/storage/storage-e48-snapshot-<DATE>
#           where date is defined by date(1) format string +%Y%m%d-%a,
#           also this currently can only be run at most once a day
#
# Note: This tool does not add these temporary mountpoints to /etc/fstab, so they
# will be lost on reboot. 
#
# number of days of snapshots to keep - If you ever shorten this,
# you'll have to remove the old snapshots manually with system-config-lvm
# or CLI tools.
days=10
################################################################################

now=`date +%Y%m%d-%a`
old=`date +%Y%m%d-%a --date="-$days days"`

# create today's snapshot
/sbin/lvcreate --size 100G -s -n storage-e48-snapshot-$now /dev/storage/e48_storage
if [ $? -eq 0 ]
then
    echo "Snapshot successfully created as /dev/storage/storage-e48-snapshot-$now"
else
    echo "Snapshot creation failed.  Exiting."
    exit 1;
fi

# create mountpoint for snapshot
/bin/mkdir -p /sftp-snapshots/$now

# mount snapshot read-only
/bin/mount -t xfs -o nouuid,nobarrier,ro,inode64,noexec /dev/storage/storage-e48-snapshot-$now /sftp-snapshots/$now
if [ $? -eq 0 ]
then
    echo "Snapshot mounted at /sftp-snapshots/$now"
else
    echo "WARNING - Could not mount snapshot."
fi

# unmount old snapshot
/bin/umount /sftp-snapshots/$old

# rmdir old snapshot mountpoint
if [ -d "/sftp-snapshots/$old" ]; then
    /bin/rmdir /sftp-snapshots/$old
fi

# delete old snapshot
/sbin/lvremove -f /dev/storage/storage-e48-snapshot-$old
if [ $? -eq 0 ]
then
    echo "Old snapshot removed. ($old)"
else
    echo "WARNING - Could not remove old snapshot. ($old)"
fi

