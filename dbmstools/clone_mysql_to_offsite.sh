#!/bin/sh
################################################################################
# MySQL Simple Clone
# Williamson - March 2014
#
# dumps local mysql database and restores the dump on a remote server.
#
# requires: 
# mysql (clent)
# pbzip2
# remote: load_mysqldump.sh 
#
# Notes:  THIS IS NOT THREAD SAFE!
#
################################################################################
# Configuration
################################################################################
remote_user=root
remote_host=poseidon-cs.tamu.edu
################################################################################

# pull in database configuration parameters
source /usr/local/share/GRIIDC/bash/db-utils.lib.sh
get_db_params /etc/griidc/db-admin.ini MYSQL_ROOT db_

export MYSQL_PWD="$db_password"

/bin/rm -f /var/tmp/mysql.dump; 
/usr/bin/mysqldump -u root --all-databases --events > /var/tmp/mysql.dump; 
/usr/bin/pbzip2 -f /var/tmp/mysql.dump; 
/usr/bin/scp /var/tmp/mysql.dump.bz2 $remote_user@$remote_host:/var/tmp/mysql.dump.bz2; 
/usr/bin/ssh $remote_user@$remote_host -C "/bin/chmod 600 /var/tmp/mysql.dump.bz2"; 
/usr/bin/ssh $remote_user@$remote_host -C "/usr/local/griidc/util/dbmstools/load_mysqldump.sh";
