#!/bin/sh
################################################################################
# MySQL Binlog Purger
# Williamson - June 2014
#
# Deletes all MySQL binary logs older than 90 days 
#
# requires: 
# mysql (clent)
# DBI::MySQL
#
################################################################################

# pull in database configuration parameters
source /usr/local/share/GRIIDC/bash/db-utils.lib.sh
get_db_params /etc/griidc/db-admin.ini MYSQL_ROOT db_

export MYSQL_PWD="$db_password"

