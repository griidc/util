#!/bin/sh
DATABASE=poseidon

echo "discovering most recent backup"
NEWEST_BACKUP=$(/bin/su barman -c "/usr/bin/barman list-backup $DATABASE" | grep -v "FAILED" | /bin/cut -f2 -d' ' | /bin/sort -r | /usr/bin/head -n 1)

BACKUP=${1:-$NEWEST_BACKUP}

echo "Latest backup discovered TAG# $NEWEST_BACKUP"
echo "Restoring backup TAG# $BACKUP"

echo "Stopping postgresql database."
/sbin/service postgresql-9.1 stop
echo "changing ownership of postgres datadir to barman, until barman is fixed upstream"
/bin/chown -R barman.barman /var/lib/pgsql-barman
echo "restoring backup locally"
/usr/bin/barman recover $DATABASE $BACKUP /var/lib/pgsql-barman/9.1/data
echo "changing ownership to postgres.postgres"
/bin/chown -R postgres.postgres /var/lib/pgsql-barman
echo "removing old data"
/bin/rm -rf /var/lib/pgsql/9.1/data
echo "moving restored database to /var/lib/pgqsl/9.1/data";
/bin/mv -f /var/lib/pgsql-barman/9.1/data /var/lib/pgsql/9.1/
echo "starting newly-refreshed postgresql database back up"
/sbin/service postgresql-9.1 start
echo "Running Post-clone SQL";

INI_FILE="/etc/opt/pelagos/db.ini"
SECTION="GOMRI_RW"
PREFIX="DB_"

# Script snippet based on:
# http://stackoverflow.com/questions/6318809/how-do-i-grab-an-ini-value-within-a-shell-script
# This does not currently handle ` in a database password, so using a 0600 root owned
# .pgpass file instead for authentication.

#eval `sed -e 's/[[:space:]]*\=[[:space:]]*/=/g' -e 's/;.*$//' -e 's/[[:space:]]*$//' -e 's/^[[:space:]]*//' -e "s/^\(.*\)=\([^\"']*\)$/$PREFIX\1=\"\2\"/" $INI_FILE | sed -n -e "/^\[$SECTION\]/,/^\s*\[/{/^[^;].*\=.*/p;}"`

echo "waiting 60 seconds for DB to fully start"
sleep 60;

echo "timestamping replicated database"
/usr/bin/psql -w gomri gomri_writer << EOF
\\c gomri;
CREATE TABLE REPLICATION_STATUS ( built_on TIMESTAMP WITH TIME ZONE );
INSERT INTO REPLICATION_STATUS ( built_on ) values (now());
EOF

# Rebuild elastica database
/bin/su pelagos -c "/opt/pelagos/bin/console fos:elastica:populate -e drupal_prod"
