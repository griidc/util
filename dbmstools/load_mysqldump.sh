#!/bin/sh
################################################################################
# MySQL Simple Clone load script - client (remote) script
# Williamson - March 2014
#
# restores database from mysqldump
#
# requires: 
# mysql (client)
# pbunzip2
#
# Notes:  THIS IS NOT THREAD SAFE!
#
################################################################################
# Configuration
logfile=/var/log/griidc/MySQL-cloning.log
################################################################################

# pull in database configuration parameters
source /usr/local/share/GRIIDC/bash/db-utils.lib.sh
get_db_params /etc/griidc/db-admin.ini MYSQL_ROOT db_

export MYSQL_PWD="$db_password"

tstamp=`date`
/usr/bin/pbunzip2 -f /var/tmp/mysql.dump.bz2
/usr/bin/mysql -u root < /var/tmp/mysql.dump
rm -f /var/tmp/mysql.dump
echo "$tstamp - loaded MySQL Dump from poseidon" >> $logfile;
