#!/usr/bin/perl -w

use strict;
use Config::IniFiles;
use File::Path qw(remove_tree);
use LWP::Simple;
use JSON;

unless (@ARGV > 0) {
    print "Must specify destination\n";
    exit;
}

my $dest = $ARGV[0];

$dest =~ s/\/+$//;

unless (-d $dest) {
    print "$dest does not exist or is not a directory\n";
    exit;
}

#my $apiUrl = 'https://data.gulfresearchinitiative.org/api/datasets?_properties=id,udi,datasetSubmission.datasetFileName,datasetSubmission.metadataFileName';
my $apiUrl = 'https://data.gulfresearchinitiative.org/api/datasets?_properties=id,udi,datasetSubmission.datasetFileName';

if (@ARGV > 1) {
    $apiUrl .= "&$ARGV[1]";
}

my $datasets = from_json(get($apiUrl));

my $conf = Config::IniFiles->new( -file => '/etc/opt/pelagos.ini');

# clean out $dest first
remove_tree($dest, {keep_root => 1});

my $ret = 0;

foreach my $dataset (@$datasets) {
    my @errors;
    # check that udi is set
    if (!exists $dataset->{'udi'} or
        !defined $dataset->{'udi'} or
        $dataset->{'udi'} =~ /^\s*$/) {
        push @errors, { type => 'udi is null or empty' };
    }
    else {
        my $datafile = $conf->val('paths', 'data_download') . "/$dataset->{udi}/$dataset->{udi}.dat";
        # check that the file exists on disk
        if (-f $datafile) {
            # check that datasetFileName is set
            if (exists $dataset->{'datasetSubmission'}->{'datasetFileName'} and
                defined $dataset->{'datasetSubmission'}->{'datasetFileName'} and
                $dataset->{'datasetSubmission'}->{'datasetFileName'} !~ /^\s*$/) {

                # create directory unless it already exists
                unless (-d "$dest/$dataset->{udi}") {
                    unless (mkdir("$dest/$dataset->{udi}")) {
                        push @errors, { type => 'could not create directory',
                                        details => [ "mkdir $dest/$dataset->{udi}" ] };
                    }
                }
                # determine file system type where UDI directory resides
                #my $fstype = `stat -f -L -c %T $dest/$dataset->{udi}`;
                #chomp($fstype);
                #if ($fstype eq 'nfs') {
                    # Give Apache write access via nfs facls
                    #system("nfs4_setfacl -a A::apache\@tamucc.edu:rwaDxtcy $dest/$dataset->{udi}");
                #}
                #else {
                    # Give Apache write access via standard facls
                    #system("setfacl -m u:apache:rwx $dest/$dataset->{udi}");
                #}
                my $linkname = "$dest/$dataset->{udi}/$dataset->{datasetSubmission}->{datasetFileName}";
                # create symlink unless it already exists
                unless (-e $linkname) {
                    unless (symlink($datafile,$linkname)) {
                        push @errors, { type => 'could not create symlink',
                                        details => [ "ln -s $datafile $linkname" ] };
                    }
                }
            }
        }
    }

    if (@errors) {
        print STDERR "\n$dataset->{udi}:\n";
        $ret = 1;
    }
    for my $error (@errors) {
        print STDERR "    $error->{type}";
        print STDERR ':' if exists $error->{'details'};
        print STDERR "\n";
        if (exists $error->{'details'}) {
            for my $line (@{$error->{'details'}}) {
                print STDERR "        $line\n";
            }
        }
    }
}

print "\n" if $ret;

exit $ret;
