#!/usr/bin/perl
########################################################################
# HTML Formatted + ASCII Mass Email Generator                          #
########################################################################
# Author: Michael S. Williamson                                        #
# Jan 2007                                                             #
# Updated: May 2014                                                    #
#                                                                      #
# Generates HTML-enabled email with inlined graphics as well as        #
# a normal ASCII message for display in non-html and ADA MUAs.         #
#                                                                      #
# Input: 3 files specified below.  2 are message, 1 is addresses       #
# Output: Mail sent to everyone in the list via smtp server            #
#                                                                      #
########################################################################
# Mailer Configuration
my $smtp_server         = 'smtp.tamucc.edu';
my $logfile             = "mailing.log";
########################################################################
# Program
use strict;
use Getopt::Long;
use Pod::Usage;
use MIME::Lite;

my $subject            = '';
my $email_from         = '';
my $reply_to           = 'griidc@gomri.org';
my $html_filename      = '';
my $text_filename      = '';
my $addresses          = '';
my $smtp               = 'smtp.tamucc.edu';
my $help               = 0;
my $man                = 0;
my $msg_delay          = 10;

GetOptions(
    'subject|s=s' => \$subject,
    'from=s' => \$email_from,
    'textfile=s' => \$text_filename,
    'addressfile=s' => \$addresses,
    'reply-to:s' => \$reply_to,
    'htmlfile:s' => \$html_filename,
    'help|?' => \$help,
    'smtp:s' => \$smtp,
    'log:s' => \$logfile,
    'delay:s' => \$msg_delay,
     man => \$man) or pod2usage(2);

pod2usage(1) if $help;
pod2usage(-verbose => 2) if $man;
pod2usage("$0: Invalid usage")  if ($subject eq '' or $email_from eq '' or $text_filename eq '' or $addresses eq '');

open LOG, ">>", $logfile or die $!;

my $textbody = read_file($text_filename);
my $date = `date +"%B %d, %Y"`;
my $tstamp = `date +"%c"`; chomp($tstamp);
my @addresses = read_file($addresses);

if ($html_filename ne '') {
    my $htmlbody = read_file($html_filename);

    foreach (@addresses) {
        $tstamp = `date +"%c"`; chomp($tstamp);
        chomp();
        my ($name,$mail) = split(/:/,$_);

        my $mailer  = MIME::Lite->new("From"        => "$email_from",
                                      "To"	        => "$name <$mail>",
                                      "Reply-To"    => "$reply_to",
                                      "Subject"     => "$subject",
                                      "Type"        => 'multipart/alternative'
                                     );
        my $plain = $mailer->attach(
		        Type    => 'text/plain',
		        Data    => "$textbody",
		        );
        my $fancy = $mailer->attach(Type => 'multipart/related');
        $fancy->attach(
		        Type    => 'text/html',
		        Data    => "$htmlbody"
		        );
        $mailer->send('smtp', $smtp_server);

        print LOG "$tstamp $name <$mail> was sent a message\n";
        print "$tstamp $name <$mail> was sent a message\n";
        sleep($msg_delay);
    }
} else {
    foreach (@addresses) {
        $tstamp = `date +"%c"`; chomp($tstamp);
        chomp();
        my ($name,$mail) = split(/:/,$_);
        my $mailer = MIME::Lite->new("From"        => "$email_from",
                                  "To"	        => "$name <$mail>",
                                  "Reply-To"    => "$reply_to",
                                  "Subject"     => "$subject",
                                  "Type"        => 'text/plain',
                                  "Data"        => "$textbody"
                                );
        $mailer->send('smtp', $smtp_server);
        print LOG "$tstamp $name <$mail> was sent a message.\n";
        print "$tstamp $name <$mail> was sent a message.\n";
        sleep($msg_delay);
    }
}
close LOG;
########################################################################
# Subroutines                                                          #
########################################################################
sub trim {
    my @out = @_;
    for (@out) {
        s/^\s+//;
        s/\s+$//;
    }
    return wantarray ? @out : $out[0];
}
sub read_file
{
    my ($file) = @_;

    local($/) = wantarray ? $/ : undef;
    local(*F);
    my $r;
    my (@r);

    open(F, "<$file") || die "open $file: $!";
    @r = <F>;
    close(F) || die "close $file: $!";

    return $r[0] unless wantarray;
    return @r;
}
########################################################################

__END__

=head1 NAME

GRiiDC mass-mail tool

=head1 SYNOPSIS

mailer.pl -subject <SUBJECT> -from <FROM> -textfile <MESSAGE.txt>
-addressfile <ADDRESSES.txt> [-htmlfile <MESSAGE.html>] [-reply-to <ADDRESS>]
[-smtp <SMTP SERVER>] [-log <logfile>] [-delay <seconds>]

=head1 OPTIONS

Required

=over 8

=item B<-subject>

subject for email, must be in quotes

=item B<-from>

send message from this address

=item F<-textfile>

filename of message in standard ASCII text

=item F<-addressfile>

filename of contacts formatted as "name":email, one per line

=back

Optional

=over 8

=item F<-htmlfile>

html formatted email (enables pretty formatting)

=item B<-reply-to>

reply-to email, defaults to griidc@gomri.org

=item B<-smtp>

smtp server, defaults to smtp.tamucc.edu

=item F<-log>

logfile, defaults to mailing.log in current directory

=item B<-delay>

seconds to wait between emails sent, defaults to 10

=back

Other:

=over 8

=item B<-help>

Print a brief help message and exits.

=item B<-man>

Prints the manual page and exits.

=back

=head1 DESCRIPTION

B<mailer.pl> will send mass-email to a list of users.

=cut
