#!/usr/bin/perl
#######################################################################
# HTML Formatted + ASCII Mass Email Generator                          #
########################################################################
# Author: Michael S. Williamson                                        #
# May 2014                                                             #
#                                                                      #
# Returns a colon-delimited list of user's names and email addresses   #
# for all users in the GRIIDC OpenLDAP database                        #
#                                                                      #
########################################################################

use strict;
use Net::LDAP;
use Term::ReadKey;

my $ldap = Net::LDAP->new( 'triton.tamucc.edu' ) or die "$@";
my $mesg = $ldap->bind( );

$mesg = $ldap->search( # perform a search
                       base   => "ou=members, ou=people, dc=griidc, dc=org",
                       filter => "(uid=*)",
                       attrs => ['cn','mail']
                     );

$mesg->code && die $mesg->error;

foreach my $entry ($mesg->entries) {
    my @name = $entry->get('cn');
    my @email = $entry->get('mail');
    $name[0]=~s/://g;
    print "\"$name[0]\":$email[0]\n";
}

$mesg = $ldap->unbind;   # take down session
