#!/usr/bin/php
<?php
# libs needed
require_once("/opt/pelagos/share/php/db-utils.lib.php");

$xml = '';

# pull record from database
$udi=null;
$udi = $argv[1];
print 'Processing '.$udi."...";

$sql = "select metadata_xml from metadata where registry_id = ?";

$dbms = OpenDB("GOMRI_RO");
$data = $dbms->prepare($sql);
$data->execute(array($udi));
$raw_data = $data->fetch();
if ($raw_data) {
    $xml = $raw_data[0];
} else {
    print "\nNo XML found in DB.\n";
    die();
}

libxml_use_internal_errors(true); // enables capture of error information
$doc = new DomDocument('1.0','UTF-8');
$tmpp = @$doc->loadXML($xml);
if (!$tmpp) {
    $err = libxml_get_last_error();
    $err_str = $err->message;
    print "Malformed XML: The XML could not be parsed. ($err_str)\n";
    die();
}

$xpath = new DOMXpath($doc);

$xpath_query = '/gmi:MI_Metadata/gmd:identificationInfo[1]/gmd:MD_DataIdentification[1]/gmd:extent[1]/gmd:EX_Extent[1]';
$extent_node_list = $xpath->query($xpath_query);

$extent_node_element = $extent_node_list->item(0);
$parent_node = $extent_node_element->parentNode;

$xpath_query_geom = '/gmi:MI_Metadata/gmd:identificationInfo[1]/gmd:MD_DataIdentification[1]/gmd:extent[1]/gmd:EX_Extent[1]/gmd:geographicElement/gmd:EX_BoundingPolygon[1]/gmd:polygon[1]/*';
$geo = $xpath->query($xpath_query_geom);
$has_geo = $geo->item(0);

if ($has_geo) {
    $orig_geom = $doc->saveXML($has_geo);
} else {
    print "No geometry found.\n";
    die();  # quick and dirty...
}

# build bounding box

$sql = "select
ST_XMIN(geom) as WestLong,
ST_XMAX(geom) as EastLong,
ST_YMAX(geom) as NorthLat,
ST_YMIN(geom) as SouthLat
from
(select ST_Envelope(ST_GeomFromGML(?, 4326)) as geom) as box;";

$envelope = '';

$data = $dbms->prepare($sql);
$rc = $data->execute(array($orig_geom));
if (!$rc) { 
    var_dump ($data->errorInfo());
    print "Could not determine envelope.\n";
    die();
}

$raw_data = $data->fetch();
if ($raw_data) {
    $west = $raw_data['westlong'];
    $east = $raw_data['eastlong'];
    $south = $raw_data['southlat'];
    $north = $raw_data['northlat'];
}

$calculated_bounding_box =
"
<gmd:EX_Extent id=\"boundingExtent\">
<gmd:geographicElement>
  <gmd:EX_GeographicBoundingBox>
    <gmd:westBoundLongitude>
      <gco:Decimal>$west</gco:Decimal>
    </gmd:westBoundLongitude>
    <gmd:eastBoundLongitude>
      <gco:Decimal>$east</gco:Decimal>
    </gmd:eastBoundLongitude>
    <gmd:southBoundLatitude>
      <gco:Decimal>$south</gco:Decimal>
    </gmd:southBoundLatitude>
    <gmd:northBoundLatitude>
      <gco:Decimal>$north</gco:Decimal>
     </gmd:northBoundLatitude>
  </gmd:EX_GeographicBoundingBox>
</gmd:geographicElement>
<gmd:geographicElement>
<gmd:EX_BoundingPolygon>
<gmd:polygon>
$orig_geom
</gmd:polygon>
</gmd:EX_BoundingPolygon>
</gmd:geographicElement>
</gmd:EX_Extent>
";

$bbox_frag = $doc->createDocumentFragment();
$bbox_frag->appendXML($calculated_bounding_box);

$extent_node_element->parentNode->removeChild($extent_node_element);

$parent_node->appendChild($bbox_frag);

$doc->normalizeDocument();
$doc->formatOutput=true;

$xml_save = $doc->saveXML();
// clean up formatting via tidy
$tidy_config = array('indent' => true,'indent-spaces' => 4,'input-xml' => true,'output-xml' => true,'wrap' => 0);
$tidy = new tidy;
$tidy->parseString($xml_save, $tidy_config, 'utf8');
$tidy->cleanRepair();

$xml_to_insert = $tidy;
$gml_to_insert = $orig_geom;


$sql = "UPDATE metadata SET geom = ST_GeomFromGML(:gml_to_insert), metadata_xml = :xml_to_insert WHERE registry_id = :udi;";
$handle = $dbms->prepare($sql);

$handle->bindParam(':gml_to_insert',$gml_to_insert);
$handle->bindParam(':xml_to_insert',$xml_to_insert);
$handle->bindParam(':udi',$udi);

$result = $handle->execute();
if (!$result) { 
    var_dump ($data->errorInfo());
    print "UPDATE SQL Failed\n";
    die();
} else {
    print "Success\n";
}

# close the barn door.
$dbms = null;
unset($dbms);


?>
