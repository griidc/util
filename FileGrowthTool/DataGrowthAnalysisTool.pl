#!/usr/bin/perl
################################################################################
# GRIIDC data growth analysis tool (filesystem scan based)
# Author: Williamson
# 20 MAY 2014
#
# This tool calculates the amount of data in the specified directory
# at the start of every month until the current month.
#
# Note: There is a hardcoded pattern match that excludes all files
# containing GridFTP in the path.  These are additional hardlinks
# created for GridFTP and SFTP distribution and should not be counted.
#
################################################################################
# Configure start date 
my $month=1;
my $year=2012;
my $data_store = "/sftp/data";
################################################################################
use strict;

my ($sec,$min,$hour,$mday,$cur_mon,$cur_year,$wday,$yday,$isdst) = localtime();
$cur_year+=1900; $cur_mon++;

my @dates;

while ($year <= $cur_year) {
    if ($year == $cur_year) {
        while ($month <= $cur_mon)  { push (@dates,"$year-$month-01"); $month++; }
    } else {
        while ($month <= 12)  { push (@dates,"$year-$month-01"); $month++; }
    }
    $year++; $month=1;
}

foreach my $date (@dates) {
    my $ts = `date --date "$date" +%s`;
    my $now = `date +%s`;
    chomp($ts); chomp($now);
    my $days = int(($now-$ts)/(3600*24)); # round down
    my @files = qx(find $data_store -type f -mtime +$days);
    my $total_size=0;
    foreach my $file (@files) {
        next if $file =~ /GridFTP/;
        chomp($file);
        my $bytes = qx(du -b "$file"); chomp($bytes);
        $total_size+=$bytes;
    }
    my $total_size_gb = int(($total_size/(1024**3))+0.5); #GB
    print "$total_size_gb GB on $date\n";
}
################################################################################

