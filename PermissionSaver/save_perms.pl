#!/usr/bin/perl
################################################################################
# saves permissions
# file/dir permission copier (applies from a reference system)
#
# Traverses a local filesystem starting at the directory specified
# in the $directories_to_search[0] variable and generates stdout
# of permission:path/file
#
# usage: first set $directories_to_search[0] to your starting dir
# then ./catalog_perms > myPermissions.perms
# result: myPermissions.perms reference file.
#
# Author: Michael Scott Williamson, Programmer III / DBA
# Texas A & M University-Corpus Christi
# November 22, 2011
#
# License: BSD (See end of file)
#
################################################################################ 
use strict;
use File::Find;
my @directories_to_search=();
$directories_to_search[0]='/u01/app/oracle/product/11.2.0/client_1';
find(\&wanted, @directories_to_search);
sub wanted {
    my $mode = (stat($File::Find::name))[2];
    printf "%04o", $mode & 07777; print ":".$File::Find::name."\n";
}
################################################################################
# Copyright © 2011, Texas A & M University-Corpus Christi                      #
# All rights reserved.                                                         #
#                                                                              #
# Redistribution and use in source and binary forms, with or without           #
# modification, are permitted provided that the following conditions are met:  #
#                                                                              #
# Redistributions of source code must retain the above copyright notice, this  #
# list of conditions and the following disclaimer.                             #
#                                                                              #
# Redistributions in binary form must reproduce the above copyright notice,    #
# this list of conditions and the following disclaimer in the documentation    #
# and/or other materials provided with the distribution.                       #
#                                                                              #
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  #
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    #
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   #
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE    #
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          #
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         #
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     #
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      #
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      #
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   #
# POSSIBILITY OF SUCH DAMAGE.                                                  #
################################################################################
