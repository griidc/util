#!/usr/bin/perl

use Digest::SHA qw(sha256_hex);
use File::stat;
use Getopt::Long;
use JSON;
use Parallel::ForkManager;
use POSIX qw(strftime);
use REST::Client;
use strict;

my $cores=12;
my $hostname = 'data.gulfresearchinitiative.org';
my $protocol = 'https';
my $filesystem_prefix='/san/data/store';
my @dataset;
my %givenDatasets;

my $big;
my $small;
my $cutoff = 25000000000; # 25 GB
my $datasetList = '';
GetOptions(
    'big' => \$big,
    'small' => \$small,
    'cutoff:i' => \$cutoff,
    'cores:i' => \$cores,
    'dataset-list:s' => \$datasetList,
);

if($big and $small) {
    print "ERROR: Use either the --big or the --small option, but not both.\n";
    exit 1;
}
if ($cutoff < 0) {
    print "ERROR: Only a positive integer may be specified for the cutoff value.\n";
    exit 1;
}
if ($cores < 1) {
    print "ERROR: You need at least 1 core to get any work done.\n";
    exit 1;
}
if ($datasetList ne '') {
    %givenDatasets = readDatasetList($datasetList);
}


sub toList {
    my $data = shift;
    my $key = shift;
    if (ref($data->{$key}) eq 'ARRAY') {
        $data->{$key};
    } elsif (ref($data->{$key}) eq 'HASH') {
        [$data->{$key}];
   } else {
        [];
   }
}

sub readDatasetList {
    my $filename = shift;
    my %hashes;
    open (my $fh, '<:encoding(UTF-8)', $filename)
        or die ("Could not open specified dataset list, $filename. Stopping.");
    while (my $row = <$fh>) {
        chomp($row);
        $hashes{"$row"}=1;
    }
    return %hashes;
}

my $apiCall = "/api/datasets?_properties=udi,datasetSubmission.datasetFileSha256Hash,datasetSubmission.datasetFileTransferStatus,datasetSubmission.datasetFileSize";
my $client = REST::Client->new();
$client->setHost("$protocol://$hostname");
my $headers = {Accept => 'application/json', charset => 'UTF-8'};
$client->GET($apiCall, $headers);
my $response = from_json($client->responseContent());
my $count=0;
my $errorCount=0;

foreach my $dataset( @$response ) {
    my $udi = $dataset->{'udi'};
    my $limitedCount = keys %givenDatasets;
    next if ($limitedCount > 0 and not $givenDatasets{"$udi"});
    my $datfile = $filesystem_prefix . '/' . $udi . '/' . $udi . '.dat';
    my $datasetSubmission = $dataset->{'datasetSubmission'};
    my $hash = $datasetSubmission->{'datasetFileSha256Hash'};
    my $status = $datasetSubmission->{'datasetFileTransferStatus'};
    my $size = $datasetSubmission->{'datasetFileSize'};
    if ($big) {
        if ($status eq 'Completed' and $size > $cutoff) {
            $count++;
            push (@dataset, $datfile . '|' . $udi . '|' . $hash);
        }
    } elsif ($small) {
        if ($status eq 'Completed' and $size <= $cutoff) {
            $count++;
            push (@dataset, $datfile . '|' . $udi . '|' . $hash);
        }
    } else {
        if ($status eq 'Completed') {
            $count++;
            push (@dataset, $datfile . '|' . $udi . '|' . $hash);
        }
    }
}

if ($big) {
    print "Hashing all $count files over $cutoff bytes using $cores CPU cores.\n";
} elsif ($small) {
    print "Hashing all $count files up to and including $cutoff bytes using $cores CPU cores.\n";
} else {
    print "Hashing all $count files using $cores CPU cores.\n";
}


my $pm = new Parallel::ForkManager($cores, '/var/tmp/');

# data structure retrieval and handling
$pm -> run_on_finish (
    sub {
        my ($pid, $exit_code, $ident, $exit_signal, $core_dump, $data_structure_reference) = @_;

        # retrieve data structure from child
        if (defined($data_structure_reference)) {  # children are not forced to send anything
            my $string = ${$data_structure_reference};  # child passed a string reference
            my ($digest, $udi, $recorded_hash, $fileSize, $fileMtime) = split(/\|/,$string);
            my $timeStr = '';
            if ($digest eq $recorded_hash) {
                print "UDI: $udi [OK] (filesize: $fileSize)\n";
            } else {
                if ($fileMtime > 0) {
                    $timeStr = strftime "%b %d, %Y", localtime($fileMtime);
                }
                print "UDI: $udi [HASH ERROR FOUND] (filesize: $fileSize, mtime: $timeStr)\n";
                $errorCount++;
            }
        } else {  # problems occuring during storage or retrieval will throw a warning
            print qq|No message received from child process $pid!\n|;
        }
    }
);

foreach my $string (@dataset) {
    my ($file,$udi,$recorded_hash) = split(/\|/,$string);
    $pm->start() and next; # do the fork
    # do work in this process
    my $return;
    my $fileSize = 0;
    my $fileMtime = 0;
    if (-f $file) {
        my $file_handle;
        my $fileDetails = stat($file);
        $fileSize = $fileDetails->size;
        $fileMtime = $fileDetails->mtime;
        open($file_handle, "<", $file);
        my $ctx = Digest::SHA->new(256);
        $ctx->addfile($file_handle);
        my $digest = $ctx->hexdigest;
        $return = $digest .'|' . $udi . '|' .$recorded_hash . '|' . $fileSize . '|'. $fileMtime;
        close $file_handle;
    } else {
        # File missing, so return blank for calculated hash, to force a failure.
        $return = '|' . $udi . '|' . $recorded_hash . '|' . $fileSize . '|' . $fileMtime;
    }
    $pm->finish(0, \$return); # do the exit in the child process
  }

$pm->wait_all_children;

if ($errorCount == 0) {
    print "[ALL OK] No problems found. Verified $count datasets against their sha256 hashes stored in the database.\n";
} else {
    print "[ERROR] Hash error found in $errorCount of the $count datasets against their sha256 hashes stored in the database.\n";
}
