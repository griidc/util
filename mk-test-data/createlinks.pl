#!/usr/bin/perl
# This script is to be called remotely from the
# production system.  It is used to build a set
# of test data as configured in the calling script
# create-test-data.sh that is part of the util repo
#
use JSON;
use REST::Client;
use strict;

my $sourcedir = '/san/data/store';
my $dest = '/san/data/download';

opendir(DIR,$sourcedir) or die "Cannot open $sourcedir\n";
my @udis = readdir(DIR);
closedir(DIR);
foreach my $udi (@udis) {
    next if ($udi =~ /^\.$/);
    next if ($udi =~ /^\.\.$/);
    # clean old links from previous run - perms may have changed
    if ( -d "$dest/$udi" ) {
        `rm -rf "$dest/$udi"`;
    }
    my $restricted = check_restricted($udi);
    if ($restricted == 0) {
        print "creating public $dest/$udi\n";
        mkdir "$dest/$udi", 0751;
        `chown pelagos.pelagos $dest/$udi`;
    } else {
        print "creating secure $dest/$udi\n";
        mkdir "$dest/$udi", 0750;
        `chown pelagos.pelagos $dest/$udi`;
    }
    opendir(UDIDIR, "$sourcedir/$udi") or die "Can't open $udi directory\n";
    my @files = readdir(UDIDIR);
    closedir(UDIDIR);
    foreach my $file (@files) {
        next if ($file =~ /^\.$/);
        next if ($file =~ /^\.\.$/);
        print "checking $file\n";
        if ( -f "$sourcedir/$udi/$file" ) {
            print "linking $file\n";
            link "$sourcedir/$udi/$file","$dest/$udi/$file";
            chmod 0644,"$dest/$udi/$file";
        }
    }
}

sub check_restricted {
    my $udi = $_[0];

    my $hostname = 'data.gulfresearchinitiative.org';
    my $protocol = 'https';

    my $apiCall = "/pelagos-symfony/api/datasets?udi=$udi";
    my $client = REST::Client->new();
    $client->setHost("$protocol://$hostname");
    my $headers = {Accept => 'application/json', charset => 'UTF-8'};
    $client->GET($apiCall, $headers);
    my $response = from_json($client->responseContent());
    # Returns 1 response.
    my $permission = 0;
    foreach my $dataset( @$response ) {
        $permission = $dataset->{'availabilityStatus'};
    }
    return (!(10 == $permission or 7 == $permission));
}

