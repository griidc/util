#!/bin/sh
######################################################################
#
# Test data generation script
#
# Author: Williamson, Michael S.
# 25 JULY 2014
#
# This script generates a tarfile of datasets in the datapath
# directory, with all datasets that have a size less than
# the maxsize parameter in the configuration.
#
# The cutfield parameter must be set so that the UDI is returned
# from the path returned by find(1) when passed to cut(1).
#
################################################################################
# Configuration
maxsize=100M
cutfield=5
datapath=/san/data/store
testsystem='griidc-dev.tamucc.edu'
################################################################################
# Program
/bin/find $datapath -size -$maxsize -type f -name "*.dat" | /bin/cut -d'/' -f$cutfield | /bin/grep ".x[0-9]\{3\}.[0-9]\{3\}:[0-9]\{4\}" | /bin/sort | /usr/bin/uniq  > /var/tmp/test.udis.txt.$$
cd $datapath
/bin/tar cfv /var/tmp/testdata.$$.tar `/bin/cat /var/tmp/test.udis.txt.$$ | /usr/bin/xargs`
/bin/rm -f /var/tmp/test.udis.txt.$$
# Send tarfile to testsystem, unpack, create dl hardlinks, etc.
scp /var/tmp/testdata.$$.tar root@$testsystem:/san/data/store
/bin/rm -f /var/tmp/testdata.$$.tar
ssh root@$testsystem  "tar xfv /san/data/store/testdata.$$.tar --directory /san/data/store"
ssh root@$testsystem  "rm -f /san/data/store/testdata.$$.tar"
ssh root@$testsystem  "/usr/local/bin/createlinks.pl"
################################################################################
