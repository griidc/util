#!/bin/sh
output=$(/sbin/lvdisplay | /bin/grep "Allocated to snapshot" -B 15 -A 7)
if [ -n "${output}" ];
    then echo "$output" | /bin/mailx -s "LVM Snapshot report" michael.williamson@tamucc.edu, patrick.krepps@tamucc.edu, roland.dominguez@tamucc.edu
fi
