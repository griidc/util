#!/bin/sh

# Shut down 172.22.10.204 interface
/sbin/ifdown 'em1'

# enable 212 for access to server for maint. access
/sbin/ifup 'em1:0'

# Prevent 204 from coming back online at boot
/bin/echo '/etc/sysconfig/network-scripts/ifcfg-em1' | /usr/bin/xargs /bin/sed -i 's/ONBOOT=yes/ONBOOT=no/g'
