#!/bin/sh
THISHOST=$(hostname)
USERNAME=$(id -un)

if [ "$USERNAME" != 'root' ]; then
    echo "This script must be run from root."
    exit 1
fi

if [ "$THISHOST" == "proteus.tamucc.edu" ]; then
    # De-activate 172.22.10.204 on proteus
    echo "De-activating Proteus's 172.22.10.204 interface"
    /sbin/ifdown 'em1:0'

    # SSH into poseidon and activate 172.22.10.204
    echo "Enabling poseidon's 172.22.10.204 interface"
    /usr/bin/ssh ipmanager@172.22.10.212  "sudo /usr/local/bin/Web-IP-Switcher/private/Enable-204.sh"
else
    echo "This utility was meant to be run from proteus.tamucc.edu only."
fi
